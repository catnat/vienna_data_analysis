import sys
import json
import argparse
import numpy as np
import pandas as pd
from pathlib import Path

repository_path = Path(__file__).resolve().parent.parent
data_path = repository_path / "data"
output_path = repository_path / "public"


def sanity_check_gis_election_data(dg, lbz):
    for bezirk_nr in range(1, 24):
        if len(dg[dg["BZ"] == bezirk_nr]) != lbz[bezirk_nr - 1]:
            print(
                "ERROR, gis data and election data don't match up for bezirk",
                bezirk_nr,
            )
            sys.exit(1)


def get_percentages(df, party_columns):
    sprengel_dict = {}
    for party in party_columns:
        party_col = party + "_percent"
        arr = (df[party] / (df["ABG."] - df["UNG."])).values
        brr = df.sprengel_name
        sprengel_dict[party] = dict(zip(brr, arr))

    return sprengel_dict


# election_dir could be 'data/wien_wahl_2015'
# election_file could be 'data/wien_wahl_2015/bv151_99999999_9999_spr.csv'
def enrich_election(election_dir, election_file):
    full_year = election_dir.name.split("_")[-1]
    year = full_year[2:]
    election_type = election_file.name[:2]

    print(f"Enriching {election_type} {full_year}...")

    # Load sprengel data: We do this once per election, because we mutate
    # the resulting data, so we would otherwise need a deepcopy for each call to
    # enrich_election().
    with open(election_dir / f"WAHLSPRGR{full_year}OGD.json", "r") as json_file:
        sprengel_json = json.load(json_file)

    # load election data
    df = pd.read_csv(election_file, sep=";")
    df.columns = df.columns.str.strip()

    # drop columns with 0 WBER (these sprengel do
    # not correspond to any sprengel in gis data)
    df = df[df["WBER"] != 0]

    # drop columns irrelevant to our evalution
    df = df.drop(
        ["NUTS1", "NUTS2", "NUTS3", "DISTRICT_CODE", "T", "WV", "WK", ""], axis=1
    )

    sprengel_name = []
    lbz = np.zeros(23)
    for bz in range(1, 24):
        sprengel_name.append([])
        for feature in sprengel_json["features"]:
            if feature["properties"]["BEZIRK"] == bz:
                sprengel = feature["properties"]["SPRENGEL"]
                if sprengel not in sprengel_name[-1]:
                    lbz[bz - 1] += 1
                    sprengel_name[-1].append(sprengel)

    # sanity check: is the number of sprengel the same in gis and election data
    sanity_check_gis_election_data(df, lbz)

    # TODO additional sanity check that compares if each gis-sprengel id corresponds
    # to one election data id

    # convert sprengel_name to format in gis data:
    # 2 digits for bezirk, 3 for sprengel
    def convert_strings(row):
        return "{:02d}{:03d}".format(row["BZ"], row["SPR"])

    df["sprengel_name"] = df.apply(convert_strings, axis=1)

    # get name list of all parties
    party_columns = [
        column
        for column in df.columns
        if column not in ["WBER", "UNG.", "ABG.", "BZ", "SPR", "sprengel_name"]
    ]

    # Calculate number of valid votes and
    # fill sprengel dict (keys: party,sprengel, values: election results)
    sprengel_dict = get_percentages(df, party_columns)

    # enrich sprengel data
    for feature in sprengel_json["features"]:
        feature["properties"]["RESULTS"] = dict()
        for party in party_columns:
            sprengel = feature["properties"]["SPRENGEL"]
            feature["properties"]["RESULTS"][party] = round(sprengel_dict[party][sprengel] * 100, 2)

    # dump enriched geojson
    output_file = output_path / f"{election_type}_{full_year}.json"
    with open(output_file, "w") as json_file:
        json.dump(sprengel_json, json_file, indent=2)
    return output_file


if __name__ == "__main__":
    enriched_paths = []

    for election_dir in data_path.iterdir():
        if not election_dir.name.startswith("wien_wahl_"):
            continue

        for election_file in election_dir.iterdir():
            if not election_file.name.endswith("_spr.csv"):
                continue

            enriched_paths.append(enrich_election(election_dir, election_file))

    election_index = {}
    for file_path in enriched_paths:
        typ, year = file_path.stem.split("_")
        if typ == "gr":
            election_index[f"Gemeinderatswahl {year}"] = f"/{file_path.name}"
        elif typ == "bv":
            election_index[f"Bezirksratswahl {year}"] = f"/{file_path.name}"
        else:
            raise ValueError(f"Unknown election type in {filename}")

    with open(output_path / "index.json", "w") as json_file:
        json.dump(election_index, json_file, indent=2)
