#!/usr/bin/env bash
set -eu

for year in 05 10 15
do
    directory="data/wien_wahl_20${year}"
    mkdir -p "${directory}"
    wget -O "${directory}/bv${year}1_99999999_9999_spr.csv" "https://www.wien.gv.at/politik/wahlen/ogd/bv${year}1_99999999_9999_spr.csv"
    wget -O "${directory}/gr${year}1_99999999_9999_spr.csv" "https://www.wien.gv.at/politik/wahlen/ogd/gr${year}1_99999999_9999_spr.csv"
    wget -O "${directory}/WAHLSPRGR20${year}OGD.json" "https://data.wien.gv.at/daten/geo\?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:WAHLSPRGR20${year}OGD&srsName=EPSG:4326&outputFormat=json"
done
