
async function fetchJSON(url) {
  const response = await fetch(url);
  return await response.json()
}

// initialize Leaflet
var map = L.map('map', {
	center: [48.2, 16.3],
	zoom: 13,
	maxBounds: L.latLngBounds(
		L.latLng(48.048, 16.137), // south-west
		L.latLng(48.386, 16.714), // north-east
	)
});


L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	maxZoom: 18,
	id: 'mapbox/light-v10',
	tileSize: 512,
	zoomOffset: -1,
	accessToken: 'pk.eyJ1IjoiY2FyaW5hayIsImEiOiJjazhkNWl6YTYwc2I0M2VtanNhYzFjNDVhIn0.xUXpESt8b6UpcBnwy3ZW9A',
}).addTo(map);

// show the scale bar on the lower left corner
L.control.scale().addTo(map);


function getColor(d) {
	return d  > 6 ? '#800026' :
		d > 5  ? '#BD0026' :
		d > 4  ? '#E31A1C' :
		d > 3  ? '#FC4E2A' :
		d > 2  ? '#FD8D3C' :
		d > 1   ? '#FEB24C' :
		d > 0   ? '#FED976' :
		'#FFEDA0';
}


function style(feature) {
	return {
		fillColor: getColor(feature.properties.RESULTS['ANDAS'] || feature.properties.RESULTS['KPOE']),
		weight: 2,
		opacity: 1,
		color: 'rgba(90,90,90,.2)',
		fillOpacity: '.4'
	};
}

function popup(f, l) {
	var out = [];
	if (f.properties){
		const results = Object.keys(f.properties.RESULTS).map(key => `
          <dt class="party">${key}</dt>
          <dd class="percent">${f.properties.RESULTS[key]}</dd>`);
		l.bindPopup(`
<h1>${f.properties.BEZIRK}. Bezirk - Sprengel ${f.properties.SPRENGEL.substring(2)}</h1>
<dl class="results">${results.join(' ')}</dl>
	`);

	}
}

fetchJSON('/index.json')
	.then(data => data['Gemeinderatswahl 2015'])
	.then(url => fetchJSON(url))
	.then(geoJSON => {
		var geojsonLayer = new L.GeoJSON(geoJSON, {style: style, onEachFeature:popup});
		geojsonLayer.addTo(map);
	})
